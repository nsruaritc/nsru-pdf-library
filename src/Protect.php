<?php
namespace Nsru\Pdf;

use Exception;
use Nsru\Hermes\File;
use Nsru\Hermes\Text;

class Protect
{
    private $accessToken = "";
    private $publicKeyFilePath  = "";

    private $exceptions = [];

    public function __construct($accessToken, $publicKeyFilePath)
    {
        $this->accessToken          = $accessToken;
        $this->publicKeyFilePath    = $publicKeyFilePath;
    }

    public function getExceptions() {
        return $this->exceptions;
    }

    public function certify($filePath, $digitalCertificateFilePath, $password)
    {
        try {
            $client = new \GuzzleHttp\Client([
                'verify' => false
            ]);
            $res = $client->request('POST', 'https://api.nsru.ac.th/pdf/sign/v1/certify', [
                'headers' => [
                    'Authorization' => "Bearer {$this->accessToken}"
                ],
                'multipart' => [
                    [
                        'name'     => 'source_file',
                        'contents' => file_get_contents($filePath),
                        'filename' => 'source_file.pdf'
                    ],
                    [
                        'name'     => 'digital_certificate',
                        'contents' => File::encrypt($digitalCertificateFilePath, $this->publicKeyFilePath)
                    ],
                    [
                        'name'     => 'digital_certificate_password',
                        'contents' => Text::encrypt($password, $this->publicKeyFilePath)
                    ]
                ],
            ]);
            $data = \json_decode( $res->getBody() );
            if(isset($data->data)) {
                return $data->data;
            } else throw new Exception('API Return with no usable data');
        } catch( Exception $e ) {
            $this->exceptions[] = $e->getMessage();
            return false;
        }
    }

}
