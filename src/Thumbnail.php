<?php
namespace Nsru\Pdf;

use Exception;

class Thumbnail
{
    private $accessToken    = "";
    private $pdf            = "";

    private $resolution         = 72;
    private $backgroundColor    = "#FFFFFF";
    private $width              = 500;
    private $height             = 500;
    private $aspectRatio        = 1;
    private $extension          = "webp";
    private $page               = 1;

    private $exceptions = [];

    public function getExceptions() {
        return $this->exceptions;
    }

    public function setSourceFile($pdfPath) {
        $this->pdf = $pdfPath;
    }

    public function setAccessToken($accessToken) {
        $this->accessToken = $accessToken;
    }

    public function setResolution($resolution) {
        $this->resolution = $resolution;
    }

    public function setBackgroundColor($backgroundColor) {
        $this->backgroundColor = $backgroundColor;
    }

    public function setScale($width, $height, $isAspectRatio) {
        $this->width        = $width;
        $this->height       = $height;
        $this->aspectRatio  = $isAspectRatio;
    }

    public function setExtension($extension) {
        $this->extension = $extension;
    }

    public function setPage($page) {
        $this->page = $page;
    }

    public function make()
    {
        try {
            $client = new \GuzzleHttp\Client([
                'verify' => false
            ]);
            $res = $client->request('POST', 'https://api.nsru.ac.th/pdf/v1/thumbnail', [
                'headers' => [
                    'Authorization' => "Bearer {$this->accessToken}"
                ],
                'multipart' => [
                    [
                        'name'     => 'source_file',
                        'contents' => file_get_contents($this->pdf),
                        'filename' => 'source_file.pdf'
                    ],
                    [
                        'name'      => 'resolution',
                        'contents'  => $this->resolution
                    ],
                    [
                        'name'      => 'background_color',
                        'contents'  => $this->backgroundColor
                    ],
                    [
                        'name'      => 'width',
                        'contents'  => $this->width
                    ],
                    [
                        'name'      => 'height',
                        'contents'  => $this->height
                    ],
                    [
                        'name'      => 'aspect_ratio',
                        'contents'  => $this->aspectRatio
                    ],
                    [
                        'name'      => 'extension',
                        'contents'  => $this->extension
                    ],
                    [
                        'name'      => 'page',
                        'contents'  => $this->page
                    ]
                ],
            ]);
            $data = \json_decode( $res->getBody() );
            if($data->is_success == 1) {
                return $data->data->image_url;
            } else throw new Exception($res->getStatusCode());
        } catch( Exception $e ) {
            $this->exceptions[] = $e->getMessage();
            return false;
        }
    }

}
