<?php
namespace Nsru\Pdf;

use Exception;

class Analyze
{
    private $accessToken    = "";
    private $pdf            = "";

    private $exceptions = [];

    public function getExceptions() {
        return $this->exceptions;
    }

    public function setSourceFile($pdfPath) {
        $this->pdf = $pdfPath;
    }

    public function setAccessToken($accessToken) {
        $this->accessToken = $accessToken;
    }

    public function analyze()
    {
        try {
            $client = new \GuzzleHttp\Client([
                'verify' => false
            ]);
            $res = $client->request('POST', 'https://api.nsru.ac.th/pdf/v1/analyze', [
                'headers' => [
                    'Authorization' => "Bearer {$this->accessToken}"
                ],
                'multipart' => [
                    [
                        'name'     => 'source_file',
                        'contents' => file_get_contents($this->pdf),
                        'filename' => 'source_file.pdf'
                    ],
                ],
            ]);
            $data = \json_decode( $res->getBody() );
            if($data->is_success == 1) {
                return new AnalyzeResult($data->data);
            } else {
                throw new Exception($data->message);
            }
        } catch( Exception $e ) {
            $this->exceptions[] = $e->getMessage();
            return false;
        }
    }

}
