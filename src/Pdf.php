<?php
namespace Nsru\Pdf;

use Exception;

class Pdf
{
    private $accessToken        = "";
    private $publicKeyFilePath  = "";

    public function __construct($accessToken, $publicKeyFilePath) {
        $this->accessToken          = $accessToken;
        $this->publicKeyFilePath    = $publicKeyFilePath;
    }

    /**
     * Undocumented function
     *
     * @return \Nsru\Pdf\Protect
     */
    public function Protect() {
        return new Protect($this->accessToken, $this->publicKeyFilePath);
    }

    /**
     * Undocumented function
     *
     * @return \Nsru\Pdf\Thumbnail
     */
    public function makeThumbnail() {
        $thumbnail = new Thumbnail();
        $thumbnail->setAccessToken($this->accessToken);
        return $thumbnail;
    }
}
