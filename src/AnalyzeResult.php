<?php
namespace Nsru\Pdf;

use Exception;

class AnalyzeResult
{
    private $sessionId;
    private $text;
    private $objects;
    private $details;

    public function __construct($data) {
        $this->sessionId    = $data->session_id;
        $this->text         = $data->text;
        $this->objects      = $data->objects;
        $this->details      = $data->details;
    }

    public function getSessionId() {
        return $this->sessionId;
    }

    public function getText() {
        return $this->text;
    }

    public function getObjects() {
        return $this->objects;
    }

    public function getDetails() {
        return $this->details;
    }
}
