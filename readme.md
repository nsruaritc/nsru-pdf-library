# README

## Basic
การใช้งานแบบที่เรียบง่ายที่สุดคือส่งไฟล์กับ Token เข้าไปโดยไม่ได้ปรับแต่งอะไร
```php
use Nsru\Pdf\Thumbnail;

$pdf = "./test/sample.pdf";

$thumbnail = new Thumbnail();
$thumbnail->setAccessToken('----------TOKEN---------');
$thumbnail->setSourceFile($pdf);
$img = $thumbnail->make();
```

## Advance
การใช้งานที่ปรับแต่งเพิ่มเติม
```php
use Nsru\Pdf\Thumbnail;

$pdf = "./test/sample.pdf";

$thumbnail = new Thumbnail();
$thumbnail->setAccessToken('----------TOKEN---------');
$thumbnail->setSourceFile($pdf);
$thumbnail->setResolution(150); // กำหนด Resolution ของรูปภาพผลลัพธ์
$thumbnail->setBackgroundColor('#FFFFFF'); // กำหนดรูปภาพพื้นหลังของรูปภาพ กรณีในไฟล์ PDF เป็นพื้นหลังโปร่งใส
$thumbnail->setScale(800, 800, true); // กำหนดขนาดของรูปภาพผลลัพธ์
$thumbnail->setExtension('jpeg'); // กำหนดนามสกุลของรูปภาพผลลัพธ์
$thumbnail->setPage(1); // เลือกหน้าที่จะทำ Thumbnail (ปกติเป็นหน้า 1)
if( $img = $thumbnail->make() ) {
    print_r( $img );
} else {
    print_r( $thumbnail->getExceptions() );
}
```